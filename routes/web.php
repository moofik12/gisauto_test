<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::group(['middleware' => ['web', 'can:shop']], function ()
{

    Route::get('/shop', 'ShopController@index')->name('shop.index');
    Route::get('/shop/add', 'ShopController@showAddPage')->name('shop.add.page');
    Route::post('/shop/add', 'ShopController@add')->name('shop.add');
    Route::get('/shop/delete/{id?}', 'ShopController@delete')->name('shop.delete');
    Route::get('/shop/orders', 'ShopController@orders')->name('shop.orders');

});

Route::group(['middleware' => ['web', 'can:orders']], function ()
{

    Route::get('/orders', 'UserController@index')->name('orders.index');;
    Route::get('/orders/add', 'UserController@showAddPage')->name('orders.add.page');
    Route::post('/orders/add', 'UserController@add')->name('orders.add');
    Route::get('/orders/delete/{id}', 'UserController@delete')->name('orders.delete');

});
