@extends('layouts.app')

@section('navbar')
    @include('shared.shop-navbar')
@endsection

@section('content')

    <div class="row">
            <div class="container text-center">
                <h3 class="section-header">Добавление товара</h3>
                <h4>Заполните поля в форме ниже</h4>
            </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('shop.add')}}" method="POST">
                {{csrf_field()}}
                <div>
                    <div class="form-group">
                        <label for="name">Наименование товара</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="price">Цена (в руб.)</label>
                        <input type="price" class="form-control" id="price" name="price" required>
                    </div>
                    <div class="form-group">
                        <label for="manufacturer_id">Производитель</label>
                        <select class="form-control" id="manufacturer_id" name="manufacturer_id">
                            @foreach($manufacturers as $manufacturer)
                                <option value="{{$manufacturer->id}}">{{ $manufacturer->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-default submit">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>  Добавить товар
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection