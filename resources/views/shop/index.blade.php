@extends('layouts.app')

@section('navbar')
    @include('shared.shop-navbar')
@endsection

@section('content')
    <div class="container">
        <div class="row col-md-8 col-md-offset-2 custyle">
            <table class="table table-striped custab">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Цена (в руб.)</th>
                    <th>Производитель</th>
                    <th class="text-center"></th>
                </tr>
                </thead>

                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->manufacturer->name }}</td>
                        <td class="text-center"><a class='btn btn-danger btn-warning' href="{{route('shop.delete', $product->id)}}">Удалить</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection