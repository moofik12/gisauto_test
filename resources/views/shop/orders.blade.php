@extends('layouts.app')

@section('navbar')
@include('shared.shop-navbar')
@endsection

@section('content')
<div class="container">

    @foreach($orders as $order=>$products)

    <h4>Заявка от пользователя {{json_decode($order)->user}}</h4>

    <div class="row col-md-10 col-md-offset-1 custyle well">
        <table class="table table-striped custab">
            <thead>
            <tr>
                <th>Название товара</th>
                <th>Цена минимальная (в руб.)</th>
                <th>Цена максимальная (в руб.)</th>
                <th>Производитель</th>
                <th class="text-center"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ json_decode($order)->name }}</td>
                <td>{{ json_decode($order)->price_min }}</td>
                <td>{{ json_decode($order)->price_max }}</td>
                <td>{{ json_decode($order)->manufacturer}}</td>
            </tr>
            </tbody>
        </table>
        <br/>
        <h4>Соответствующие заявке товары:</h4>
        <div class="row col-md-8 col-md-offset-2 custyle">
            <table class="table table-striped custab">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Цена (в руб.)</th>
                    <th>Производитель</th>
                    <th class="text-center"></th>
                </tr>
                </thead>

                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->manufacturer->name }}</td>
                        <td class="text-center"><a class='btn btn-success' href="">Откликнуться на заявку</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="clearfix"></div><br/>
    @endforeach
</div>
@endsection