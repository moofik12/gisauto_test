<h2>У вашего магазина есть товары соответствующие данной заявке</h2>
<h4>Содержимое заявки</h4>
<table class="table table-striped custab">
    <thead>
    <tr>
        <th>Название товара</th>
        <th>Цена минимальная (в руб.)</th>
        <th>Цена максимальная (в руб.)</th>
        <th>Производитель</th>
        <th class="text-center"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $order->name }}</td>
        <td>{{ $order->price_min }}</td>
        <td>{{ $order->price_max }}</td>
        <td>{{ $order->manufacturer->name}}</td>
    </tr>
    </tbody>
</table>