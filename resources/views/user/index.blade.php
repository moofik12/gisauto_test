@extends('layouts.app')

@section('navbar')
    @include('shared.user-navbar')
@endsection

@section('content')
    <div class="container">
        <div class="row col-md-10 col-md-offset-1 custyle">
            <table class="table table-striped custab">
                <thead>
                <tr>
                    <th>Наименование</th>
                    <th>Цена минимальная (в руб.)</th>
                    <th>Цена максимальная (в руб.)</th>
                    <th>Производитель</th>
                    <th>Cтатус заявки</th>
                    <th class="text-center"></th>
                </tr>
                </thead>

                @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->price_min }}</td>
                        <td>{{ $order->price_max }}</td>
                        <td>{{ $order->manufacturer->name }}</td>
                        <td>{{ $order->translated_status }}</td>
                        <td class="text-center"><a class='btn btn-danger btn-warning' href="{{
                        route('orders.delete', $order->id)}}">Удалить</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection