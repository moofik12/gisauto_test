<nav class="navbar navbar-default">
    <div class="container">
        <div class="row">

            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li @if(\Request::route()->getName() == 'orders.index') class="active" @endif ><a href="{{ route('orders.index')  }}">Заявки</a></li>
                    <li @if(\Request::route()->getName() == 'orders.add.page') class="active" @endif><a href="{{ route('orders.add.page') }}">Добавить заявку</a></li>
                    <li><a href="{{ route('logout') }}">Выход</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>