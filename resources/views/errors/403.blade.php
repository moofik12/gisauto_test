@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2 text-center">
            <div class="logo">
                <h1>403</h1>
            </div>
            <p class="lead text-muted">Упс, кажется произошла ошибка. Доступ запрещен!</p>
            <div class="clearfix"></div>

            <div class="sr-only">
                &nbsp;

            </div>
            <br>
            <div class="col-lg-6 col-lg-offset-3">
                <div class="btn-group btn-group-justified">
                    <a href="{{ route('login') }}" class="btn btn-info btn-block">Вернуться в личный кабинет</a>
                </div>
            </div>
        </div>
    </div>
@endsection