<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderEmailed extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    public $subject = 'New orders for your shop';


    public function __construct($order)
    {
        $this->order = Order::find($order->id)->first();
    }

    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.orders.index', ['order' => $this->order]);
    }
}
