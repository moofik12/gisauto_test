<?php

namespace App\Helpers;


use App\Models\Order;
use Illuminate\Database\DatabaseManager;
use ReflectionClass;
use function explode;
use function json_decode;
use function json_encode;
use function preg_match;
use function str_replace;

/**
 * Class OrderHelper
 * @property DatabaseManager $database
 * @package App\Helpers
 */
class OrderHelper
{
    /**
     * @var DatabaseManager $database
     */
    protected $database;

    public function __construct(DatabaseManager $databaseManager)
    {
        $this->database = $databaseManager;
    }

    /**
     * Получает текстовое значение статуса переданой модели
     * @param $model
     * @param $status
     * @return mixed
     */
    public function translateStatus($model, $status)
    {
        $translation = $this->database->table('translations')
            ->where('model', $model)
            ->where('status', $status)
            ->first()->translation;
        return $translation;
    }


    /**
     * Возвращает массив, где ключами являются json-ифицированные объекы Order с доп. полями
     * а значениями ключей, соответстующие этому order товары из списка тех, что передали в функцию
     * в качестве аргумента
     * @param $products - товары, переданные в функцию в качестве аргумента
     * @return array
     */
    public function returnSatisfiableOrders($products)
    {
        $orders = Order::where('status', '<>', Order::STATUS_CATCHED)
            ->where('status', '<>', Order::STATUS_CONFIRMED)->get();

        $result = [];

        foreach ($products as $product) {
            foreach ($orders as $order) {
                $explodedOrderName = explode(' ', $order->name);
                $orderNameRegex = $this->regexifyNameAttr($order->name);

                if(preg_match($orderNameRegex, $product->name)
                    && $product->price <= $order->price_max
                    && $product->price >= $order->price_min
                    && $product->manufacturer_id == $order->manufacturer_id) {
                    $json = json_decode(json_encode($order));
                    $json->manufacturer = $product->manufacturer->name;
                    $json->status = $this->translateStatus(
                        (new ReflectionClass($order))->getName(), $order->status);
                    $json->user = $order->user->name;
                    $json = json_encode($json);
                    $result[$json][] = $product;
                }
            }
        }

        return $result;
    }

    /**
     * Проверяет подходит ли пользователю-магазину определенная заявка
     * @param $user - Пользователь-магазин
     * @param $order - Переданная заявка
     * @return bool
     */
    public function isOrderSuitableForShop($user, $order)
    {
        $products = $user->products;
        $explodedOrderName = explode(' ', $order->name);
        $orderNameRegex = $this->regexifyNameAttr($order->name);

        foreach ($products as $product) {
            if (
                preg_match($orderNameRegex, $product->name)
                && $product->price <= $order->price_max
                && $product->price >= $order->price_min
                && $product->manufacturer_id == $order->manufacturer_id
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Превращает строку в регулярное выражение, в котором все пробелы заменены на
     * Возможную комбинацию любых символов между вхождениями
     * @param $name
     * @return string
     */
    private function regexifyNameAttr($name)
    {
        return '/' . str_replace(' ', '.*?', $name . '.*') . '/ius';
    }
}

