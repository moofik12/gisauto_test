<?php

namespace App\Helpers\Auth;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\DatabaseManager;

/**
 * Class AuthHelper
 * @property Role $role
 * @property DatabaseManager $queryBuilder
 * @package Moofik\Kitty\Services\Auth
 */
class AuthHelper
{
    private $role;
    private $currentUser;

    /**
     * Проверяет роль пользователя - магазин или обычный пользователь системы
     */
    public function checkUserRole($param, $value)
    {
        /**
         * @var User $user
         */
        $user = User::where($param, $value)->get()->first();
        $this->currentUser = $user;

        if (!empty($user)) {
            $this->role = $user->role;
        }
    }

    /**
     * Where to redirect users after login (куда редиректить пользователя после логина)
     *
     * @return string Url
     */
    public function redirectTo()
    {
        if (empty($this->role)) {
            return '/';
        }
        else if ($this->role->isShop()) {
            return '/shop';
        }
        else {
            return '/orders';
        }
    }

    public function getCurrentUserRole()
    {
        return $this->role;
    }
}