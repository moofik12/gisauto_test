<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.10.17
 * Time: 4:33
 */

namespace App\Helpers;


use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


/**
 * Class RabbitMQHelper
 * @property AMQPChannel $channel
 * @property AMQPStreamConnection $connection
 * @package App\Helpers
 */
class RabbitMQHelper
{
    protected $connection;
    protected $channel;

    public function __construct()
    {
        if(env('RABBITMQ_RUN', false)) {
            $this->connection = new AMQPStreamConnection(
                env('RABBITMQ_HOST'),
                env('RABBITMQ_PORT'),
                env('RABBITMQ_LOGIN'),
                env('RABBITMQ_PASSWORD'),
                env('RABBITMQ_VHOST'));
            $this->channel = $this->connection->channel();
            $this->channel->queue_declare(env('RABBITMQ_QUEUE'),
                false, false, false, false);
        }
    }

    public function rabbitConsume($callback)
    {
        if( !empty($this->getChannel()) && !empty($this->getConnection()) ) {
            $this->channel->basic_consume(env('RABBITMQ_QUEUE'),
                '', false, false, false, false,
                function ($data) use ($callback) {
                    $callback($data);
                });

            echo " [x] Awaiting Email Jobs requests\n";

            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }

            $this->channel->queue_delete(env('RABBITMQ_QUEUE'));
            $this->channel->close();
            $this->connection->close();
        }
        else {
            echo ("\nRabbitMQ mode is offline. Please add RABBITMQ_RUN=true to your .env file.\n");
        }
    }

    public function rabbitProduce($callback)
    {
        if( !empty($this->getChannel()) && !empty($this->getConnection()) ) {
            $message = $callback();

            $msg = new AMQPMessage($message);
            $this->channel->basic_publish($msg, '', env('RABBITMQ_QUEUE'));

            return true;
        }
        else {
            echo ("\nRabbitMQ mode is offline. Please add RABBITMQ_RUN=true to your .env file.\n");
        }
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @return AMQPStreamConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }
}