<?php

namespace App\Console\Commands;


use App\Helpers\OrderHelper;
use App\Helpers\RabbitMQHelper;
use App\Jobs\MailConsumer;
use App\Mail\OrderEmailed;
use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use function json_decode;


class RabbitMQMailer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmqmailer';

    /**
     * @var string
     */
    protected $description = 'Runs mail sender for RabbitMQ email jobs queue';

    protected $rabbitMQ;

    protected $helper;

    protected $mailer;

    /**
     * RabbitMqMailer constructor.
     * @param RabbitMQHelper $rabbitMQ
     * @param OrderHelper $orderHelper
     * @param Mail $mail
     */
    public function __construct(RabbitMQHelper $rabbitMQ, OrderHelper $orderHelper, Mail $mail)
    {
        parent::__construct();
        $this->rabbitMQ = $rabbitMQ;
        $this->helper = $orderHelper;
        $this->mailer = $mail;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $this->rabbitMQ->rabbitConsume(function ($data) {
                $order = json_decode($data->body);
                $users = User::where('role_id', Role::SHOP)->get();
                foreach ($users as $user) {
                    if($this->helper->isOrderSuitableForShop($user, $order)) {
                        $this->info('sending email to ' . $user->email);
                        Mail::to([$user->email])->send(new OrderEmailed($order));
                    }
                }
            });
        }
        catch (\Exception $exception) {
            $this->info('Rabbit consumer could not connect. Check RabbitMQ ENV credentials.');
            $this->info($exception->getMessage());
        }
    }


}
