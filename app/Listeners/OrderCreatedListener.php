<?php


namespace App\Listeners;

use App\Events\Event;
use App\Events\OrderCreated;
use App\Helpers\RabbitMQHelper;
use Illuminate\Queue\SerializesModels;
use function json_encode;

class OrderCreatedListener
{
    use SerializesModels;
    /**
     * EventListener constructor.
     */
    protected $rabbitMQ;

    public function __construct(RabbitMQHelper $rabbitMQHelper)
    {
        $this->rabbitMQ = $rabbitMQHelper;
    }

    /**
    * Handle the event.
    * @param  OrderCreated  $event
    */
    public function handle(OrderCreated $event)
    {
        $order = $event->getOrder();
        $this->rabbitMQ->rabbitProduce(function () use ($order) {
            return json_encode($order);
        });
    }
}