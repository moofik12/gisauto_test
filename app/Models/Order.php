<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use function app;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const STATUS_REGISTERED = '1';
    const STATUS_CATCHED = '2';
    const STATUS_CONFIRMED = '3';

    protected $orderHelper;

    protected $fillable = [
        'name', 'price_min', 'price_max', 'manufacturer_id', 'status'
    ];

    public function manufacturer()
    {
        return $this->hasOne('App\Models\Manufacturer', 'id', 'manufacturer_id');
    }

    public function products()
    {
        return Product::where('name', 'LIKE', '%'.$this->name.'%')
            ->where('price', '<=', $this->price_max)
            ->where('price', '>=', $this->price_min)
            ->where('manufacturer_id', $this->manufacturer()->id)->get();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function getHelper()
    {
        return $this->orderHelper;
    }

    protected function getTranslatedStatusAttribute()
    {
        $model = __CLASS__;
        return app()->make('App\Helpers\OrderHelper')
            ->translateStatus($model, $this->status);
    }
}
