<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserProduct extends Pivot
{
    protected $fillable = ['user_id', 'product_id'];

    protected $table = 'user_product';

    public $primaryKey = null;

    public $incrementing = false;
}
