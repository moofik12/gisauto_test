<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'manufacturer_id'];

    public function shops()
    {
        return $this->belongsToMany('App\Models\User', 'user_product')
            ->using('App\Models\UserProduct');
    }

    public function manufacturer()
    {
        return $this->hasOne('App\Models\Manufacturer', 'id', 'manufacturer_id');
    }
}
