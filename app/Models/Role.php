<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 28.09.17
 * Time: 13:15
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const SHOP = 1;
    const USER = 2;

    protected $fillable = ['name', 'is_shop'];

    public function users()
    {
        return $this->hasMany('Moofik\Kitty\Models\User');
    }

    public function isShop()
    {
        return (bool)$this->is_shop;
    }
}