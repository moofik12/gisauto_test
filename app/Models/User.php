<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const CREDENTIALS_USERNAME = 'login';
    const CREDENTIALS_PASSWORD = 'password';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function products()
    {
        if($this->role->isShop()) {
            return $this->belongsToMany('App\Models\Product', 'user_product')
                ->using('App\Models\UserProduct');
        }
        else {
            return null;
        }
    }

    public function orders()
    {
        if(empty($this->products())) {
            return $this->hasMany('App\Models\Order', 'user_id', 'id');
        }
    }

    public function getSuitableOrdersAttrubute()
    {
        return $this->products()->orders;
    }
}
