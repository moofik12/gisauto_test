<?php

namespace App\Http\Controllers;


use App\Events\OrderCreated;
use App\Models\Manufacturer;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $userId = Auth::id();
        /**
         * @var User $shop
         * @var Collection $products
         */
        $user = User::where('id', $userId)->first();
        $orders = $user->orders;

        return view('user.index')->with([
            'orders' => $orders
        ]);
    }

    public function showAddPage(Request $request)
    {
        $manufacturers = Manufacturer::all();
        return view('user.add', ['manufacturers' => $manufacturers]);
    }

    public function add(Request $request)
    {
        $input = $request->all();
        $orderName = $input['name'];
        $orderPriceMin = $input['price_min'];
        $orderPriceMax = $input['price_max'];
        $productManufacturerId = $input['manufacturer_id'];

        try{
            $order = User::find(Auth::id())->orders()->create([
                'name' => $orderName,
                'price_min' => $orderPriceMin,
                'price_max' => $orderPriceMax,
                'manufacturer_id' => $productManufacturerId
            ]);
        }
        catch (ModelNotFoundException $e) {
            return redirect()->back()->with([
                'error' => 'Заявка не была добавлена.'
            ]);
        }

        event(new OrderCreated($order));

        return redirect()->back()->with(['success' => 'Заявка была успешно добавлена.']);
    }

    public function delete($id)
    {
        try {
            Order::find($id)->delete();
        }
        catch (\Exception $e) {
            return redirect()->back()->with([
                'error' => 'Не удалось удалить заявку.'
            ]);
        }

        return redirect()->back()->with(['success' => 'Заявка была успешно удалена.']);
    }

}