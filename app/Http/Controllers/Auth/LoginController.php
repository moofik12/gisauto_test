<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Auth\AuthHelper;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $authHelper;

    public function __construct(AuthHelper $authHelper)
    {
        $this->authHelper = $authHelper;
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('guest.auth.login');
    }

    public function username()
    {
        return 'login';
    }

    public function redirectTo()
    {
        $userId = $this->guard()->id();
        $this->authHelper->checkUserRole('id', $userId);
        return $this->authHelper->redirectTo();
    }
}
