<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Auth\AuthHelper;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $authHelper;

    /**
     * RegisterController constructor.
     * @param AuthHelper $authHelper
     */
    public function __construct(AuthHelper $authHelper)
    {
        $this->authHelper = $authHelper;
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('guest.auth.register')->with([
            'typesOfUser' => Role::all()
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'login' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'exists:roles,id'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'login' => $data['login'],
            'role_id' => $data['role_id'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function redirectTo()
    {
        $userId = $this->guard()->id();
        $this->authHelper->checkUserRole('id', $userId);
        return $this->authHelper->redirectTo();
    }
}
