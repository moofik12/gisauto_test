<?php


namespace App\Http\Controllers;

use App\Helpers\OrderHelper;
use App\Jobs\NewOrderNotifier;
use App\Models\Manufacturer;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function view;

class ShopController extends Controller
{
    /**
     * @var User $shop
     */
    protected $shop;
    protected $orderHelper;

    public function __construct(OrderHelper $orderHelper)
    {
        $this->orderHelper = $orderHelper;
    }

    public function index(Request $request)
    {
        $shop = User::where('id', Auth::id())->first();
        $products = $shop->products;

        return view('shop.index')->with([
                'products' => $products
            ]);
    }

    public function delete($id)
    {
        try {
            Product::find($id)->delete();
        }
        catch (\Error $e) {
            return redirect()->back()->with([
                    'error' => 'Не удалось удалить товар.'
                ]);
         }

        return redirect()->back()->with(['success' => 'Товар был успешно удален.']);
    }

    public function showAddPage(Request $request)
    {
        $manufacturers = Manufacturer::all();
        return view('shop.add', ['manufacturers' => $manufacturers]);
    }

    public function add(Request $request)
    {
        $input = $request->all();
        $productName = $input['name'];
        $productPrice = $input['price'];
        $productManufacturerId = $input['manufacturer_id'];

        try{
            User::find(Auth::id())->products()->create([
                'name' => $productName,
                'price' => $productPrice,
                'manufacturer_id' => $productManufacturerId
            ]);
        }
        catch (ModelNotFoundException $e) {
            return redirect()->back()->with([
                'error' => 'Не удалось добавить товар.'
            ]);
        }

        return redirect()->back()->with(['success' => 'Товар был успешно добавлен.']);
    }

    public function orders(Request $request)
    {
        $shop = User::where('id', Auth::id())->first();
        $products = $shop->products;
        $ordersToProducts = $this->orderHelper->returnSatisfiableOrders($products);

        return view('shop.orders', ['orders' => $ordersToProducts]);
    }
}
