<?php

namespace App\Http\Middleware;

use App\Helpers\Auth\AuthHelper;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    protected $authHelper;

    public function __construct(AuthHelper $authHelper)
    {
        $this->authHelper = $authHelper;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $this->authHelper->checkUserRole('id', Auth::id());
            return response()->redirectTo( $this->authHelper->redirectTo() );
        }
        return $next($request);
    }
}
