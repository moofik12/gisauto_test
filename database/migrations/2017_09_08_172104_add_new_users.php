<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;

class AddNewUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roleShop = Role::where('name', 'shop')->get()->first()->id;
        User::create(['name'=>'Admin', 'surname'=>'Admin',
            'login'=>'shop1', 'email'=>'shop1@test.ru',
            'password'=>bcrypt('shop1'), 'role_id'=>$roleShop]);
        User::create(['name'=>'Admin', 'surname'=>'Admin',
            'login'=>'shop2', 'email'=>'shop2@test.ru',
            'password'=>bcrypt('shop2'), 'role_id'=>$roleShop]);

        $roleUser = Role::where('name', 'user')->get()->first()->id;
        User::create(['name'=>'User', 'surname'=>'User',
            'login'=>'user', 'email'=>'guest@test.ru',
            'password'=>bcrypt('user'), 'role_id'=>$roleUser]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('login', '=', 'shop1')
            ->delete();
        User::where('login', '=', 'shop2')
            ->delete();
        User::where('login', '=', 'user')
            ->delete();
    }
}
