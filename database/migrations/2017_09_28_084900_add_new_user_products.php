<?php

use App\Models\UserProduct;
use Illuminate\Database\Migrations\Migration;

class AddNewUserProducts extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserProduct::create(['user_id' => 1, 'product_id' => 1]);
        UserProduct::create(['user_id' => 1, 'product_id' => 2]);
        UserProduct::create(['user_id' => 1, 'product_id' => 3]);
        UserProduct::create(['user_id' => 1, 'product_id' => 4]);
        UserProduct::create(['user_id' => 2, 'product_id' => 5]);
        UserProduct::create(['user_id' => 2, 'product_id' => 6]);
        UserProduct::create(['user_id' => 2, 'product_id' => 7]);
        UserProduct::create(['user_id' => 2, 'product_id' => 8]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        UserProduct::where('user_id', 1)
            ->where('product_id', 1)->delete();
        UserProduct::where('user_id', 1)
            ->where('product_id', 2)->delete();
        UserProduct::where('user_id', 1)
            ->where('product_id', 3)->delete();
        UserProduct::where('user_id', 1)
            ->where('product_id', 4)->delete();
        UserProduct::where('user_id', 2)
            ->where('product_id', 5)->delete();
        UserProduct::where('user_id', 2)
            ->where('product_id', 6)->delete();
        UserProduct::where('user_id', 2)
            ->where('product_id', 7)->delete();
        UserProduct::where('user_id', 2)
            ->where('product_id', 8)->delete();
    }
}
