<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;

class AddNewProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Product::create(['name' => 'шина автомобильная R17',
            'price' => 10000,
            'manufacturer_id' => 1
        ]);
        Product::create(['name' => 'шина автомобильная R15',
            'price' => 8500,
            'manufacturer_id' => 1
        ]);
        Product::create(['name' => 'шина автомобильная R15',
            'price' => 8800,
            'manufacturer_id' => 2
        ]);
        Product::create(['name' => 'шина автомобильная R14',
            'price' => 8500,
            'manufacturer_id' => 1
        ]);
        Product::create(['name' => 'свечи зажигания 4х4',
            'price' => 5600,
            'manufacturer_id' => 1
        ]);
        Product::create(['name' => 'свечи зажигания 4х4',
            'price' => 5200,
            'manufacturer_id' => 2
        ]);
        Product::create(['name' => 'свечи зажигания 4х4',
            'price' => 5200,
            'manufacturer_id' => 3
        ]);
        Product::create(['name' => 'свечи зажигания 4х3',
            'price' => 4200,
            'manufacturer_id' => 3
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Product::where('name','шина автомобильная R17')
            ->where('manufacturer_id', 1)->delete();
        Product::where('name','шина автомобильная R15')
            ->where('manufacturer_id', 1)->delete();
        Product::where('name','шина автомобильная R15')
            ->where('manufacturer_id', 2)->delete();
        Product::where('name', 'шина автомобильная R14')
            ->where('manufacturer_id', 1)->delete();
        Product::where('name','свечи зажигания 4х4')
            ->where('manufacturer_id', 1)->delete();
        Product::where('name','свечи зажигания 4х4')
            ->where('manufacturer_id', 2)->delete();
        Product::where('name','свечи зажигания 4х4')
            ->where('manufacturer_id', 3)->delete();
        Product::where('name','свечи зажигания 4х3')
            ->where('manufacturer_id', 3)->delete();
    }
}
