<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;

class AddNewRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::create(['name' => 'shop', 'is_shop' => true]);
        Role::create(['name' => 'user', 'is_shop' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::where('name', '=', 'shop')->delete();
        Role::where('name', '=', 'user')->delete();
    }
}
