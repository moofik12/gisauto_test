<?php

use App\Models\Manufacturer;
use Illuminate\Database\Migrations\Migration;

class AddNewManufacturers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Manufacturer::create(['name'=>'Toyota']);
        Manufacturer::create(['name'=>'BMW']);
        Manufacturer::create(['name'=>'Mitsubishi']);
        Manufacturer::create(['name'=>'Moskvich']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Manufacturer::where('name', 'Toyota')->delete();
        Manufacturer::where('name', 'BMW')->delete();
        Manufacturer::where('name', 'Mitsubishi')->delete();
        Manufacturer::where('name', 'Moskvich')->delete();
    }
}
