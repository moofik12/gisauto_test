<?php

use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAndFillTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->integer('status');
            $table->string('translation');
            $table->timestamps();
        });

        $orderReflection = new ReflectionClass(Order::class);
        DB::table('translations')->insert([
            'model' => $orderReflection->getName(),
            'status' => Order::STATUS_REGISTERED,
            'translation' => 'Заявка сформирована'
        ]);
        DB::table('translations')->insert([
            'model' => $orderReflection->getName(),
            'status' => Order::STATUS_CATCHED,
            'translation' => 'Ожидает подтверждения о'
        ]);
        DB::table('translations')->insert([
            'model' => $orderReflection->getName(),
            'status' => Order::STATUS_CONFIRMED,
            'translation' => 'Заявка согласована с'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('and_full_translation');
    }
}
